#include "communications.h"

static void sendChar(unsigned char c)
{
	signed char k;
	unsigned char v;

	for (k = 1; k >= 0; k--) {
		v = (c >> (4 * k)) & 0xf;
		v = (v < 10) ? (v + '0') : (v - 10 + 'a');
		x_putchar(v);
	}
}

static void sendShort(unsigned short s)
{
	sendChar(s >> 8);
	sendChar(s & 0xff);
}

static void sendLong(unsigned long l)
{
	sendShort(l >> 16);
	sendShort(l & 0xffff);
}

static void sendLLong(unsigned long long ll)
{
	sendLong(ll >> 32);
	sendLong(ll & 0xffffffff);
}

void sendHexa(unsigned long long d, tIntType bitlength)
{
#ifdef PUTS_HEXA
	x_putchar('0');
	x_putchar('x');
#endif

	switch (bitlength) {
	case CHAR:
		sendChar((unsigned char)(d));
		break;
	case SHORT:
		sendShort((unsigned char)(d));
		break;
	case LONG:
		sendLong((unsigned char)(d));
		break;
	default:
		sendLLong((unsigned long long)d);
	}
}

void sendDecimal(signed long long d)
{
	signed long long ds = d;

	if ((ds / 10) != 0) {
		sendDecimal(d / 10);
		if (d > 0)
			x_putchar((d % 10) + '0');
		else
			x_putchar(((-d) % 10) + '0');
	} else if (d >= 0) {
		x_putchar((d % 10) + '0');
	} else if (d < 0) {
		x_putchar('-');
		x_putchar((-(d) % 10) + '0');
	}
}

static unsigned char compareDoubles(double A, double B, double epsilon)
{
	double diff = A - B;
	return (diff < epsilon) && (-diff < epsilon);
}

void sendFloat(double f, tPrecision p)
{
	signed long long i = (signed long long)f;
	unsigned char j = 0, k;

	// floor
	sendDecimal(i);
	f = (f - i) * p;
	i = (f >= 0) ? (signed long long)f : -(signed long long)f;
	f = (f >= 0) ? f : -f;

	// float
	putchar('.');
	while ((f < (float)p) && !compareDoubles(f, (double)p, 1 / (double)p)) {
		j++;
		p /= 10;
	}
	for (k = 1; k < j; k++)
		x_putchar('0');
	if (f - i >= 0.5f)
		i++;
	sendDecimal(i);
}

void send32hex(unsigned long long t)
{
	unsigned char v;
	signed char k;

#ifdef PUTS_HEXA
	x_putchar('0');
	x_putchar('x');
	x_putchar('0');
	x_putchar('x');
#endif
	for (k = 12; k >= 0; k--) { // 64 bit per 5 -> 13 sections
		v = (t >> (5 * k)) & 0x1f;
		// using extended hexadecimal aplhabet: 0123456789abcdefghijklmnopqrstuv
		v = (v < 10) ? (v + '0') : (v - 10 + 'a');
		x_putchar(v);
	}
}

void send64hex(unsigned long long x)
{
	unsigned char v;
	signed char k;

#ifdef PUTS_HEXA
	x_putchar('0');
	x_putchar('x');
	x_putchar('0');
	x_putchar('x');
	x_putchar('0');
	x_putchar('x');
#endif
	for (k = 10; k >= 0; k--) { // 64 bit per 6 -> 11 sections
		v = (x >> (6 * k)) & 0x3f;
		// using Bash alphabet: 01234456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@_
		v = (v < 10) ? (v + '0') :
		    (v < 36) ? (v - 10 + 'a') :
		    (v < 62) ? (v - 36 + 'A') :
		    (v < 63) ? '@' :
			       '_';
		x_putchar(v);
	}
}
