// include here for the equivalent putchar method of your embedded communication

// define YOUR putchar method
#define x_putchar(c) uart_putc(c)

// if needded, define below to have a '0x' printed in front of the hexa format
#define PUTS_HEXA

typedef enum { CHAR, SHORT, LONG, LLONG } tIntType;

typedef enum {
	DEC1 = 10,
	DEC2 = 100,
	DEC3 = 1000,
	DEC4 = 10000,
	DEC5 = 100000,
	DEC6 = 1000000
} tPrecision;

void sendHexa(unsigned long long d, tIntType bitlength);
void sendDecimal(signed long long d);

void sendFloat(double f, tPrecision precision);

void send32hex(unsigned long long t);
void send64hex(unsigned long long x);
