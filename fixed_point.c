#include "fixed_point.h"
#include "communications.h"

signed addFixed(signed a, signed b)
{
	return (a + b);
}

signed mulFixed(signed a, signed b)
{
	signed long long tmp;
	tmp = (signed long long)a * (signed long long)b;
	tmp /= SCALE;
	return (signed)tmp;
}

signed divFixed(signed a, signed b)
{
	signed long long tmp = (signed long long)a * SCALE;
	if (b != 0)
		tmp /= (signed long long)b;
	return (signed)tmp;
}

unsigned sqrtFixed(unsigned y)
{
	// sqrt on fixed point computation using Newton convergence algorithm
	unsigned x = 1 * SCALE; // avoid 0
	unsigned x_old = 0;
	while ((x - x_old) != 0) {
		x_old = x;
		x = addFixed(divFixed(x, 2 * SCALE),
			   divFixed(y, mulFixed(2 * SCALE, x)));
	}
	return x;
}

void displayFixed(signed r)
{
	unsigned long p = SCALE;
	unsigned char j = 0, k;
	if (r < 0) {
		x_putchar('-');
		r = -r;
	}

	// unit
	sendDecimal((signed long long)(r / SCALE));
	// decimal
	x_putchar('.');
	r = r - (r / SCALE) * SCALE;

	// leading decimal zeros
	while (r < p) {
		j++;
		p /= 10;
	}
	for (k = 1; k < j; k++)
		x_putchar('0');
	sendDecimal((signed long long)r);
}
