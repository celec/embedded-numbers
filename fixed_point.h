#define SCALE 1000

signed addFixed(signed a, signed b);
signed mulFixed(signed a, signed b);
signed divFixed(signed a, signed b);
unsigned sqrtFixed(unsigned y);
void displayFixed(signed r);
