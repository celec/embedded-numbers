# Extended number handle on embedded C

## Communication of formatted numbers without libC

Without access to the standard library---if you don't want to use any any stubs---, use a `putchar` method to communicate and print numbers from your embedded system to your terminal.

On the header, include your equivalent `putchar` (for any digital communication protocol), and define its function name below. You can also choose to have a `0x` print in front of hexadecimal format.

### API

Format to signed or unsigned hexadecimal (for negative, its print with the 2's complement convention). Choose in function of the wanted type `{CHAR, SHORT, LONG, LLONG}`:
```C
void sendHexa(unsigned long long d, tIntType bitlength);
```

Format to a decimal print. If negative, it shows a `-` in front of the number.
```C
void sendDecimal(signed long long d);
```

Format a floating point value, in decimal, with the liberty of the float precision (using `DEC1..6` for the number of decimal digits).
```C
void sendFloat(double f, tPrecision precision);
```

### Unusual: print on base 32 ad 64 format using extended hexadecimal alphabet
```C
void send32hex(unsigned long long t);
void send64hex(unsigned long long x);
```
If you choose to print `0x` in front of the hexadecimal values, it will print here `0x0x` for base 32 and `0x0x0x` for 64.

## Fixed point arithmetic
If your MCU doesn't support floating point computation (FPU), you can always use fixed point implementation using a `SCALE` factor of your choice (in `fixed_point.h`). This library provides the 3 elemental operations: addition, multiplication, and division.
```C 
signed addFixed(signed a, signed b);
signed mulFixed(signed a, signed b);
signed divFixed(signed a, signed b);
```
The display of the real value, after descaling, use the previous number communication API with a dedicated function:
```C 
void displayFixed(signed r);
```

As a bonus, a squared root computation using the Newton's convergence algorithm is also implemented for fixed point arithmetic:
```C 
unsigned sqrtFixed(unsigned y);

int main(void)
{
        signed y = 2 * SCALE;
        displayFixed(sqrtFixed(y));
}
```
Note that for fixed point computation, you have to **always scale first** your variables before calling the operation functions.
